#if defined _check_player
	#endinput
#endif

#define _check_player

#include <amxmodx>
#include <sqlx>

public Load(id)
{		
	ResetPlayer(id);
	
	get_user_authid(id, g_player_data[id][PD_AUTHID], 21)
	get_user_ip(id, g_player_data[id][PD_IP], 21, 1);
	
	if(!is_valid_steamid(g_player_data[id][PD_AUTHID]))
		formatex(g_player_data[id][PD_AUTHID], charsmax(g_player_data[][]), "ERROR_STEAM%d%d",random_num(1,1000), random_num(1,1500))

	if(is_user_bot(id) || is_user_hltv(id))
		return;

	new flg[4];
	get_pcvar_string(g_Cvars[CVAR_VIP_FLAG], flg, charsmax(flg))
	
	if( strlen(flg) > 0 && get_user_flags(id) & read_flags(flg)) set_bit(g_vip, id);
	else	clear_bit(g_vip, id)

	new query[256], Data[1]; Data[0] = id;
	formatex(query, charsmax(query), "SELECT * FROM `%s` WHERE (`player_id` = '%s' OR `player_ip` = '%s') LIMIT 1",\
	TABLE, g_player_data[id][PD_AUTHID], g_player_data[id][PD_IP])
		
	SQL_ThreadQuery(g_Sqlx, "QueryLoadPlayer", query, Data, 1)
}
public QueryLoadPlayer(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	new id = Data[0];
	
	if(SQL_NumResults(Query) <= 0)
		return SQL_FreeHandle(Query);
		
	g_player_stats[id][LOAD] = true;
	g_player_stats[id][ID] =  SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"id"));
	
	g_player_stats[id][EXP] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_exp"));
	g_player_stats[id][MEDAL] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_medal"));

	g_player_stats[id][CASES] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_cases"));
	g_player_stats[id][KEYS] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_keys"));

	g_player_stats[id][LEVEL] = LevelUpdate(id,true);
	
	g_player_stats[id][COINS] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_coins"));
	g_player_stats[id][TOTAL_COINS] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_coins_total"));

	g_player_stats[id][WISHES] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_wishes"))
	g_player_stats[id][WISH_TIME] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_wish_time"))

	g_player_stats[id][SET] = SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_set"))
 
	new weapons[WEAPON_SIZE*2+10];
	SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_weapon_skin"), weapons, charsmax(weapons))

	trim(weapons)

	new text[Q_ITEM_SIZE];
	SQL_ReadResult(Query, SQL_FieldNameToNum(Query,"player_items"), text, charsmax(text))

	trim(text)

	new Trie:trie = TrieCreate();
	new skin[6];
	
	while ( strlen(text) ) {
		strbreak(text, skin, charsmax(skin), text, charsmax(text))
		
		if (!TrieKeyExists(trie, skin)) TrieSetCell(trie, skin, str_to_num(skin))
	}

	new skin_id;
	for(new i = 0; i<WEAPON_SIZE; i++)
	{
		strbreak(weapons, skin, charsmax(skin), weapons, charsmax(weapons))
		skin_id = str_to_num(skin); 

		if ( skin_id == 0 || !isSkinInWeaponSlot(skin_id, i) || !TrieKeyExists(trie, skin)) {
			g_player_stats[id][WP_SKIN][i] = 0;
		} else g_player_stats[id][WP_SKIN][i] = skin_id;

	}

	TrieDestroy(trie)

	checkSetStatus(id)
	
	return SQL_FreeHandle(Query);
}

SavePlayer(id)
{
	if(is_user_bot(id) || is_user_hltv(id) || !is_user_connected(id))
		return;

	new query[1024];
	
	new weapon_skins[WEAPON_SIZE*3 + 10]
	for(new i; i<WEAPON_SIZE-1;i++)
		formatex(weapon_skins, charsmax(weapon_skins),"%s %d", weapon_skins, g_player_stats[id][WP_SKIN][i])

	new name[32]; get_user_name(id, name, charsmax(name));
	MakeStringSQLSafe(name, name, 31)
	
	if(g_player_stats[id][LOAD])
	{
		new len = formatex(query, charsmax(query), "UPDATE `%s` SET `player_exp` = '%d', `player_medal` = '%d', `player_weapon_skin`='%s',\
		",TABLE, g_player_stats[id][EXP], g_player_stats[id][MEDAL], weapon_skins)
		len += formatex(query[len], charsmax(query) - len, "`player_cases`='%d', `player_keys`='%d', `player_coins`='%d', `player_coins_total`='%d', `player_wishes` = '%d', `player_wish_time` = '%d',"\
		,g_player_stats[id][CASES],g_player_stats[id][KEYS],g_player_stats[id][COINS],g_player_stats[id][TOTAL_COINS], g_player_stats[id][WISHES], g_player_stats[id][WISH_TIME])
		len += formatex(query[len], charsmax(query) - len, " `player_set` = '%d' WHERE (`id` = '%d')"\
		,g_player_stats[id][SET],g_player_stats[id][ID])
	}
	else
	{
		
		new len = formatex(query, sizeof(query) - 1, "INSERT INTO `%s` ( `player_name`, `player_id`, `player_ip`, `player_exp`, `player_medal`, `player_cases`, `player_keys`, `player_weapon_skin`, `player_wish_time` )", \
			TABLE);
			
		len += formatex(query[len], charsmax(query) - len, " VALUES ( '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%s', '%d' );",\
		 name,g_player_data[id][PD_AUTHID], g_player_data[id][PD_IP], g_player_stats[id][EXP],g_player_stats[id][MEDAL],\
		 g_player_stats[id][CASES],g_player_stats[id][KEYS],weapon_skins, !g_player_stats[id][WISH_TIME] ? get_systime(0) : g_player_stats[id][WISH_TIME])
	}

	new Data[1]; Data[0] = id;
	SQL_ThreadQuery(g_Sqlx, "QuerySavePlayer", query, Data, 1)
}
public QuerySavePlayer(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
	{
		return SQL_Error(Query, Error, Errcode, FailState);
	}
	
	return SQL_FreeHandle(Query);
}
