#if defined _querysql_included
	#endinput
#endif

#define _querysql_included

#include <amxmodx>
#include <sqlx>

const TABLE_MAX_LEN = 1024

new check_map

new const TABLE_NAME[] = "CSRank"
stock MainSQL()
{
	new query[TABLE_MAX_LEN], len;
	
	len = formatex(query, charsmax(query), "CREATE TABLE IF NOT EXISTS `CSRank` (\
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,\
	`player_name`  varchar(32) NOT NULL,\
	`player_id` varchar(32) NOT NULL,\
	`player_ip` varchar(32) NOT NULL,\
	`player_sid` varchar(32) NOT NULL,\
	`player_exp` int(10) NOT NULL,\
	`player_medal` int(10) NOT NULL,\
	`player_items` TEXT NOT NULL,\
	`player_weapon_skin` varchar(255) NOT NULL,");
	len += formatex(query[len], charsmax(query)-len, "\
	`player_cases` int(10) NOT NULL,\
	`player_keys` int(10) NOT NULL,\
	`player_coins` int(10) NOT NULL,\
	`player_coins_total` int(10) NOT NULL,\
	`player_smart_cases` TEXT NOT NULL, \
		PRIMARY KEY (`id`) )\
	ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;")
	
	SQL_ThreadQuery(g_Sqlx, "QueryTemkCreate", query)

	//DeletePlayers();
}
stock DeletePlayers()
{
	new query[512];
	formatex(query, charsmax(query), "DELETE FROM `%s` WHERE (UNIX_TIMESTAMP(NOW()) - `player_wish_time` >= 3600*%d*1 AND `player_wish_time` > 0)", TABLE_NAME, get_pcvar_num(g_Cvars[CVAR_DELETE_DAY]))

	SQL_ThreadQuery(g_Sqlx,"QueryTemkDelete", query)
}
public QueryTemkDelete(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
		return SQL_Error(Query, Error, Errcode, FailState);

	return SQL_FreeHandle(Query)
}
public QueryTemkCreate(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	if(FailState)
		return SQL_Error(Query, Error, Errcode, FailState);
		
	check_map ++;

	if(check_map == 1)
	{
		new query[128];
		formatex(query, charsmax(query), "show columns FROM `%s` where `Field` = 'player_wishes'", TABLE_NAME );
	
		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query)
	}
	else if(check_map == 2)
	{
		if(SQL_NumResults(Query) > 0)
			return SQL_FreeHandle(Query);
		
		new query2[128];
		formatex(query2, charsmax(query2), "ALTER TABLE `%s` ADD `player_wish_time` INT(10) NOT NULL AFTER `player_smart_cases`;",TABLE_NAME );

		new query[128];
		formatex(query, charsmax(query), "ALTER TABLE `%s` ADD `player_wishes` INT(10) NOT NULL AFTER `player_smart_cases`;",TABLE_NAME );

		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query)
		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query2)


	} else if(check_map == 4)
	{
		new query[128];
		formatex(query, charsmax(query), "show columns FROM `%s` where `Field` = 'player_set'", TABLE_NAME );
	
		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query)
	}
	else if(check_map == 5)
	{
		if(SQL_NumResults(Query) > 0)
			return SQL_FreeHandle(Query);
		
		new query2[128];
		formatex(query2, charsmax(query2), "ALTER TABLE `%s` ADD `player_set` INT(10) NOT NULL AFTER `player_wishes`;",TABLE_NAME );

		new query[128];
		formatex(query, charsmax(query), "ALTER TABLE `%s` ADD `player_promo` varchar(128) NOT NULL AFTER `player_wishes`;",TABLE_NAME );

		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query)
		SQL_ThreadQuery(g_Sqlx,"QueryTemkCreate", query2)
	}
	
	return SQL_FreeHandle(Query);
}
