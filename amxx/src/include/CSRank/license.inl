#if defined _license_included
    #endinput
#endif

#define _license_included

#include <amxmisc>
#include <sockets>
	
stock bool: license ( id = 0 )
{
	if(!get_license() && checkLicense)
	{
		new textLicense[128];
		formatex(textLicense, charsmax(textLicense), "Problem with license, check addons/amxmodx/logs/%s/error", PLUGIN_NAME)
		if( !id ) server_print(textLicense);
		else 	  client_print(id, print_chat, textLicense)
		
		return false;
	}
	
	return true;
}