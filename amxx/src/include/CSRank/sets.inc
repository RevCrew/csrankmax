#if defined _sets_included
	#endinput
#endif

#define _sets_included

#include <amxmodx>
#include <hamsandwich>

#include <ColorChat>

enum SetsReward
{
    SETS_REWARD_NONE = 0,

    SETS_REWARD_HP,
    SETS_REWARD_DMG,
    SETS_REWARD_EXP,
    SETS_REWARD_RSP,

    SETS_REWARD_HE
}

enum _:SetsArray {
    SET_ID,
    Array:SET_ITEMS,
    SET_NAME[64],
    SET_BONUS,
    SET_BONUS_COUNT,
    SET_REQ_MEDALS,
    SET_CLASS
}

static const sSetsReward[][] = 
{
    "",
    "HP",
    "DMG",
    "EXP",
    "RSP",
    "HE"
}

new Array: g_Sets

new SETS_INI_TAG[] = "[SET]";
new SETS_INI_FILE[] = "Sets.ini";

new sSetsFile[64];

new g_bRevivedOnce[33];

public setsEventInit() {
    //Register Spawn
	RegisterHam( Ham_Spawn, "player", "FwdPlayerSpawn", 1 );

	//Register Kill
	RegisterHam(Ham_Killed, "player", "FwdPlayerDeath", 1);

	//Register Take Damage
	RegisterHam(Ham_TakeDamage, "player", "FwdPlayerTakeDMG");

	//Register Round Start
	register_logevent( "eRound_start", 2, "1=Round_Start" );
}

initSets() {


    getDirByType(DIR_CONFIG, sSetsFile, charsmax(sSetsFile), SETS_INI_FILE);

    if ( !file_exists(sSetsFile) )
        return PrintMessage("[SETS] Ini file not found [%s]", sSetsFile)

    g_Sets = ArrayCreate(SetsArray)

    parseSets( g_Sets )
    return 0
}

public eRound_start() {
    new p[32],c, id;
    get_players(p,c, "ch");

    for(new i; i<c; i++) {
        id = p[i];

        g_bRevivedOnce[id] = false;
    }
}
public FwdPlayerTakeDMG(id, inflictor, attacker, Float:damage, damagebits) {
    if ( attacker == id || !csr_is_valid_player(attacker) || !g_player_stats[attacker][SET]) return;

    new data[SetsArray];
    ArrayGetArray(g_Sets, getSetByID(g_player_stats[attacker][SET]), data);

    if ( data[SET_BONUS] == _:SETS_REWARD_DMG ) {
        new Float:fMult = data[SET_BONUS_COUNT] * 0.01 + 1.0;
        SetHamParamFloat( 4, damage * fMult)
    }
}   
setCheckExp(id) {
    if (!g_player_stats[id][SET] ) return 0

    new data[SetsArray];
    ArrayGetArray(g_Sets, getSetByID(g_player_stats[id][SET]), data);

    if ( data[SET_BONUS] == _:SETS_REWARD_EXP ) {
        return data[SET_BONUS_COUNT]
    }

    return 0
}

public FwdPlayerSpawn(id) {
    if ( !is_user_alive(id) || !g_player_stats[id][SET]) return HAM_IGNORED;

    new data[SetsArray];
    ArrayGetArray(g_Sets, getSetByID(g_player_stats[id][SET]), data);

    if ( data[SET_BONUS] == _:SETS_REWARD_HP ) {
        set_user_health(id, get_user_health(id) + data[SET_BONUS_COUNT]);
    }

    if ( data[SET_BONUS] == _:SETS_REWARD_HE ) {
        set_task( 1.25, "GiveHeBonus", id + 33134)
        
    }

    return HAM_IGNORED;
}
public GiveHeBonus(id) {
    id -=33134;

    if ( !is_user_alive(id)) return;
    give_item(id, "weapon_hegrenade")
}

public FwdPlayerDeath(id, killer, glib) {
    if (!g_player_stats[id][SET]) return

    new data[SetsArray];
    ArrayGetArray(g_Sets, getSetByID(g_player_stats[id][SET]), data);

    if ( data[SET_BONUS] == _:SETS_REWARD_RSP ) {
        new team = get_user_team(id);

        if ( !g_bRevivedOnce[id] && ( team == 1 || team == 2) ) {
            if (random_num(1,100) < data[SET_BONUS_COUNT]) {
                //Set respwan in 1 second
				set_task(1.0, "Task_Respawn", id);
            }
        }
    }
}

public Task_Respawn(iPlayer)
{
	//Respawn Player
    ExecuteHamB(Ham_CS_RoundRespawn, iPlayer);
    new szName[33]; get_user_name(iPlayer, szName, charsmax(szName))
    Print( 0, "%L", LANG_PLAYER, "CSRANK_SET_RESPAWN_TEXT",szName)

	//client_cmd(0,"spk %s",respawn);
	//Disable respawn until next round
    g_bRevivedOnce[iPlayer] = true;
}

public ShowSetsMenu(id) {

    if ( !isFeatureEnabled(id, g_Sets) )    return

    new menu, title[150];
    getMenuTitle( title, charsmax(title))
    
    new currSet = getSetByID( g_player_stats[id][SET] )
    new setName[64], data[SetsArray];
    new bonusStr[16];

    if ( currSet < 0 ) formatex(setName, charsmax(setName), "\r%L", id, "CSRANK_SET_EMPTY")
    else {
        ArrayGetArray(g_Sets, currSet, data);

        getSetBonusStr( data[SET_BONUS], data[SET_BONUS_COUNT], bonusStr, charsmax(bonusStr))
        formatex(setName, charsmax(setName), "%s%s \w[\r%s\w]",getSetColorByClass(data[SET_CLASS]), data[SET_NAME],bonusStr)
    }

    formatex(title, charsmax(title),"%s%L^n%L",title,\
	 id, "CSRANK_SETS_MENU_TITLE", id, "CSRANK_SET_CURRENT", setName)
    
    menu = menu_create(title, "HandleSetsMenu")

    new size = ArraySize(g_Sets)
    new sSetID[6];
    for(new i; i< size; i++) {
        ArrayGetArray(g_Sets, i, data)

        num_to_str(data[SET_ID], sSetID, charsmax(sSetID))

        getSetBonusStr( data[SET_BONUS], data[SET_BONUS_COUNT], bonusStr, charsmax(bonusStr))
        formatex(title, charsmax(title),"%s%s \w[\r%s\w] - %s[%L]",getSetColorByClass(data[SET_CLASS]), data[SET_NAME],bonusStr,
        g_player_stats[id][MEDAL] >= data[SET_REQ_MEDALS] ? "\w" : "\d", id, "CSRANK_SET_REQ_MEDALS", data[SET_REQ_MEDALS])
        menu_additem(menu, title, sSetID)
    }

    menu_setprop(menu, MPROP_PERPAGE, 6)
    menu_display(id, menu, 0);
}
public HandleSetsMenu(id, menu, item) {
    if (item == MENU_EXIT) {
        ShowMainMenu(id)
        return menu_destroy(menu);
    }
        
    new data[6], _dummy[1];
    new access, callback;
    menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);
    
    new setID = str_to_num(data);
    ShowOneSetMenu(id, setID, bool:(setID == g_player_stats[id][SET]))
    return menu_destroy(menu);
}

ShowOneSetMenu(id, setID, bool:current = false) {
    new menu, title[150];
    getMenuTitle( title, charsmax(title))
    
    new currSet = getSetByID(setID)
    new data[SetsArray]; ArrayGetArray(g_Sets, currSet, data);

    new setName[64];
    new bonusStr[16];
    getSetBonusStr( data[SET_BONUS], data[SET_BONUS_COUNT], bonusStr, charsmax(bonusStr))
    formatex(setName, charsmax(setName), "%s%s \w[\r%s\w] - %s[%L]",getSetColorByClass(data[SET_CLASS]), data[SET_NAME],bonusStr,
    g_player_stats[id][MEDAL] >= data[SET_REQ_MEDALS] ? "\w" : "\d", id, "CSRANK_SET_REQ_MEDALS", data[SET_REQ_MEDALS])

    formatex(title, charsmax(title),"%s%L^n%L",title,\
	 id, "CSRANK_SETS_MENU_TITLE", id, "CSRANK_SET_CHOOSE", setName)
    
    menu = menu_create(title, "HandleSetMenu")

    formatex(title, charsmax(title), "%s%L %s^n", g_player_stats[id][MEDAL] >= data[SET_REQ_MEDALS] && checkSetPlayer(id, setID, false) ? "\w" : "\d", id, "CSRANK_SET_CHOOSE_SET",
    current ? "\r*" : "")

    new sSetID[8];
    formatex(sSetID, charsmax(sSetID), "0_%d",setID)
    menu_additem(menu, title, sSetID)

    new size = ArraySize(data[SET_ITEMS])
    
    new _data[Items]
    for(new i; i< size; i++) {
        ArrayGetArray(g_Items, search_array(ArrayGetCell(data[SET_ITEMS],i)), _data)
        num_to_str(i+1, sSetID, charsmax(sSetID))

        formatex(title, charsmax(title),"%s%s", (_data[ITEM_CLASS] == 4) ? "\y" : _data[ITEM_CLASS] == 3 ? "\r" : _data[ITEM_CLASS] == 2 ? "\w" : "\d",_data[ITEM_NAME]);
        menu_additem(menu, title, sSetID)
    }

    return menu_display(id, menu, 0);
}
public HandleSetMenu(id, menu, item) {
    if (item == MENU_EXIT) {
        ShowSetsMenu(id)
        return menu_destroy(menu);
    }
        
    new data[8], _dummy[1];
    new access, callback;
    menu_item_getinfo(menu, item, access, data,5, _dummy, charsmax(_dummy), callback);
    
    new set[8]; copy(set, 7, data)

    switch (set[0]) {
        //Choose Set
        case '0': {
            replace(set, charsmax(set), "0_", "")

            new setID = str_to_num(set);
            if ( !checkSetPlayer(id, setID, false) ) {
                client_print_color(id, DontChange, "%s %L", CHAT_PREFIX, id, "CSRANK_SET_NO_MEDALS")
                return menu_destroy(menu);
            }

            g_player_stats[id][SET] = setID
            client_print_color(id, DontChange, "%s %L", CHAT_PREFIX, id, "CSRANK_SET_CHOOSE_SUCCESS", setID)
        }
    }
    return menu_destroy(menu);
}

stock checkSetStatus(id) {

    if ( !is_user_connected(id) || !g_player_stats[id][SET] || checkSetPlayer(id, -1, true))
        return

    g_player_stats[id][SET] = 0;
}

public checkSetPlayer( id, set_id, bool:checkSelf ) {
    new data[SetsArray];

    if ( !checkSelf )   ArrayGetArray(g_Sets, getSetByID(set_id), data);
    else                ArrayGetArray(g_Sets, getSetByID(g_player_stats[id][SET]), data);

    if ( g_player_stats[id][MEDAL] < data[SET_REQ_MEDALS] )
        return false;

    new size = ArraySize(data[SET_ITEMS]), cell, bool:has = false
    for(new i; i< size; i++) {
        // WeaponID 
        cell = ArrayGetCell(data[SET_ITEMS], i);
        has = false;
        // Check that player the same ID
        for (new j; j < WEAPON_SIZE; j++)
            if (g_player_stats[id][WP_SKIN][j] == cell) {
                has = true;
                break;
            }

        if ( !has ) {
            return false;
        }
    }

    return true;
}

stock getSetBonusStr( bonus, bonusCount, output[], len ) {
    
    for(new i = 1; i< _:SetsReward; i++) {
        if ( bonus != i) continue;

        formatex(output, len, "%d %s", bonusCount, sSetsReward[_:i])
    }
}


stock getSetBonusFromStr( bonus[] ) {
    for(new i; i< sizeof(sSetsReward); i++) {
        if (equali(bonus, sSetsReward[i])) {
            return i;
        }
    }

    return -1;
}
stock getSetColorByClass( class ) {
    new ret[3];

    switch (class) {
        case 4: ret = "\y";
        case 3: ret =  "\r";
        case 2: ret =  "\w";
        default: ret = "\d";
    }

    return ret
}

stock getSetByID( id ) {
    if ( !id || id < 0 ) return -1;

    new size = ArraySize(g_Sets)
    new data[SetsArray]
    for(new i; i<size; i++) {
        ArrayGetArray(g_Sets, i, data)

        if ( data[SET_ID] == id ) return i;
    }

    return -1;
}

stock parseSets( &Array: eSets) {
    new f = fopen( sSetsFile, "r"); 

    new filedata[128], data[SetsArray]
    new _id[6],_bonus[12], _count[6], _medal[6], _class[6];

    new bool:index = false;
    while (!feof(f)) {
        fgets(f, filedata, charsmax(filedata));
        trim(filedata);

        if (!filedata[0] || filedata[0] =='#') continue;

        if (equali(filedata, SETS_INI_TAG, strlen(SETS_INI_TAG))) {
            index = !index;

            if ( !index ) {
                #if defined CSRANK_MAX_DEBUG
                //server_print("ID[%d]NAME[%s][BONUS][%d][COUNT][%d][MEDAL][%d][CLASS][%d]",
                //            data[SET_ID], data[SET_NAME],data[SET_BONUS], data[SET_BONUS_COUNT], data[SET_REQ_MEDALS], data[SET_CLASS])

               // server_print("ITEMS:")
                //for(new i; i< ArraySize(data[SET_ITEMS]); i++) {
                 //   server_print("%d:[%d]", i, ArrayGetCell(data[SET_ITEMS], i))
                //}

                #endif



                ArrayPushArray(eSets, data)
                continue;
            }

            replace_all(filedata, charsmax(filedata), SETS_INI_TAG, "");
            trim(filedata);

            parse(filedata, _id, charsmax(_id), data[SET_NAME], 63, _bonus, charsmax(_bonus),
                _count, charsmax(_count), _medal, charsmax(_medal), _class, charsmax(_class));

            data[SET_ID] = str_to_num(_id);
            data[SET_BONUS] = getSetBonusFromStr(_bonus)

            if (data[SET_BONUS] == -1) {
                PrintMessage("[SETS] Unkown bonus as [%s][%s]", filedata, _bonus)
            }

            data[SET_BONUS_COUNT] = str_to_num(_count)
            data[SET_REQ_MEDALS] = str_to_num(_medal)
            data[SET_CLASS] = str_to_num(_class)

            data[SET_ITEMS] = _:ArrayCreate(1)
            continue;
        }

        if ( !str_to_num(filedata) ) PrintMessage("[SETS] Unkown options as [%s]", filedata)

        ArrayPushCell(data[SET_ITEMS], str_to_num(filedata))

    }

    PrintMessage("Load %d Set(s)", ArraySize(eSets))
} 
