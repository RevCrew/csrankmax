#if defined _wishes_included
	#endinput
#endif

#define _wishes_included

#include <amxmodx>

enum _:WishesReward
{
    WISHES_REWARD_NONE = 0,

    WISHES_REWARD_EXP,
    WISHES_REWARD_CASE,
    WISHES_REWARD_KEY,
    WISHES_REWARD_COIN,

    WISHES_REWARD_SCASE,
    WISHES_REWARD_SKIN
}

static const _szWish_reward[WishesReward][] = 
{
    "",
    "exp",
    "case",
    "key",
    "coin",
    "smart_case",
    "skin"
}

stock const WISHES_INI_TAG[] = "[WEEK]";

/*
    Give a wishes reward to player
*/

CheckWishes(id)
{
    new sys = get_systime(0);

    if ( g_player_stats[id][WISH_TIME] != 0 && g_player_stats[id][WISH_TIME] - sys > 0) return;

    g_player_stats[id][WISH_TIME] = sys + 3600*24*1; // one day
    g_player_stats[id][WISHES] += get_pcvar_num(g_Cvars[CVAR_WISH_ADD]);

    new count = get_pcvar_num(g_Cvars[CVAR_WISH_ADD]);
    if (get_bit(g_vip, id) ) {
        g_player_stats[id][WISHES] += 1; 
        count +=1;
    }
        
    client_print_color(id, DontChange, "%s %L", CHAT_PREFIX, id, "CSRANK_WISHES_DAY_BONUS", count);
    CmdScreenFade(id, 3, {100,100,100}, 150)
}

public wishes_give_reward_to_player( Handle:sqlx, const id,  Array:gl_wishes, &chance, reward[], len )
{
    if ( !is_user_connected(id) || ArraySize(gl_wishes) <= 0 || g_player_stats[id][WISHES] <= 0)   return ;

    new Array:wishes[WishesData];
    ArrayGetArray(gl_wishes, 0, wishes)

    new rnd = random_num(0, 100);

    chance = 100;

    new data[WishData], size = ArraySize(wishes[WISHES_ITEMS]), take = -1;
    for (new i; i< size; i++)
    {
        ArrayGetArray( wishes[WISHES_ITEMS], i, data);

        if ( rnd <= data[WISHES_CHANCE] ) //
        {
            take = i;

            chance = data[WISHES_CHANCE];
            break;
        }
    }

    if ( take == -1)
    {
        wishes_get_reward( id, WISHES_REWARD_NONE, 0, reward, len);
        return;
    }

    wishes_get_reward(id, data[WISHES_REWARD], data[WISHES_COUNT], reward, len);
    wishes_add_reward(sqlx, id, data[WISHES_REWARD], data[WISHES_COUNT])

    new x[3];
    for(new i; i<3; i++)    x[i] = random_num(1,255);
    CmdScreenFade(id, 3, x, 100)
}
public wishes_add_reward( Handle:sqlx, id, const reward, const count)
{
    new cid = count;

    switch (reward)
    {
        case WISHES_REWARD_NONE:   return;
        case WISHES_REWARD_EXP:    
        {
            g_player_stats[id][EXP] += count;
            g_player_stats[id][LEVEL] = LevelUpdate(id, false);
        }
        case WISHES_REWARD_CASE:
        {
            g_player_stats[id][CASES] += count;
        }
        case WISHES_REWARD_KEY:
        {
            g_player_stats[id][KEYS] += count;
        }
        case WISHES_REWARD_COIN:
        {
            g_player_stats[id][COINS] += count;
            g_player_stats[id][TOTAL_COINS] += count;
        }

        case WISHES_REWARD_SCASE:
        {
            scase_oper_player( sqlx, id, cid, OperationType:OPER_ADD_ITEM)
        }
        case WISHES_REWARD_SKIN:
        {
            item_add_to_player( sqlx, id, cid, OperationType:OPER_ADD_ITEM)
        }
        
    }
}
public wishes_get_reward( id, const reward, const count, str[], len)
{
    switch (reward)
    {
        case WISHES_REWARD_NONE:   formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_NONE" )
        case WISHES_REWARD_EXP:    formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_EXP", count )
        case WISHES_REWARD_CASE:   formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_CASE", count )
        case WISHES_REWARD_KEY:    formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_KEY", count )
        case WISHES_REWARD_COIN:    formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_COIN", count )

        case WISHES_REWARD_SCASE:    formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_SCASE", count )
        case WISHES_REWARD_SKIN:    formatex(str, len, "%L", id, "CSRANK_WISHES_RWD_SKIN", count )
    }
}

/* 
    Read wishes to array from file
*/
public wishes_read_config ( Array: wishes )
{
    new fileWishes[64]; getDirByType(DIR_CONFIG, fileWishes, charsmax(fileWishes), "Wishes.ini")

    if ( !file_exists(fileWishes) )
		return PrintMessage("[Wish], wishes config file not found, check [%s]", fileWishes)

    new f = fopen( fileWishes, "r");
    if ( !f )	return PrintMessage("Can't open file for read [%s]", fileWishes);

    new filedata[256], bool:index;
    new data[WishData], _data[WishesData], lChance[6], lReward[6], lCount[6];

    while (!feof(f))
	{
        fgets(f, filedata, charsmax(filedata));
        trim(filedata);

        if(!filedata[0] || filedata[0] == '#')  continue;
        
        if(equali(filedata, WISHES_INI_TAG, strlen(WISHES_INI_TAG)))
		{
            index = !index;

            if ( !index )
            {
                SorWishes( _data[WISHES_ITEMS] );
                ArrayPushArray(wishes, _data); 
                continue;
            }

            replace(filedata, charsmax(filedata), WISHES_INI_TAG, "");
            trim(filedata)

            copy(_data[WISHES_DATA_NAME], charsmax(_data[WISHES_DATA_NAME]), filedata);
            _data[WISHES_ITEMS] = _:ArrayCreate(WishData);

            for(new i = 0; i<WishData; i++) data[i] = _:0;   
            continue;
        }

        if ( filedata[0] != '"' )   return PrintMessage("[Wishes] Unkown options at [%s]", filedata);    
        parse(filedata, lChance, charsmax(lChance), lReward, charsmax(lReward), lCount, charsmax(lCount));

        if ( lChance[strlen(lChance) - 1] == '%' )  replace(lChance, charsmax(lChance), "%", "");

        data[WISHES_CHANCE] = str_to_num(lChance);

        for( new i = WISHES_REWARD_NONE; i< WishesReward; i++) if (equali(lReward, _szWish_reward[i]) )    data[WISHES_REWARD] = i;

        data[WISHES_COUNT] = str_to_num(lCount);

        ArrayPushArray(_data[WISHES_ITEMS], data);
	}


    PrintMessage("Load %d Wish(es)", ArraySize(wishes))
    return fclose(f);
}

stock SorWishes( Array: wishes, OrderType: order = ORDER_ASC )
{
	new Data[1]; Data[0] = _:order;
	ArraySort(wishes, "SortWishesbyChance", Data, 1)
}

public SortWishesbyChance(Array: array, item1, item2, const data[], data_size)
{
	new OrderType:order = OrderType: data[0];

	new item_value[2][WishData];

	ArrayGetArray(array, item1, item_value[0]);
	ArrayGetArray(array, item2, item_value[1]);

	new chance1 = item_value[0][WISHES_CHANCE];
	new chance2 = item_value[1][WISHES_CHANCE];

	switch (order)
	{
		case ORDER_ASC:
		{
			if(chance1 > chance2)	return 1;
			else					return -1;
		}
		case ORDER_DESC:
		{
			if(chance1 > chance2)	return -1;
			else					return 1;
		}
	}

	return 0;
}