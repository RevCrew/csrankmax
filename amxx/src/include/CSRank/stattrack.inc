#if defined _stat_track_included
	#endinput
#endif

#define _stat_track_included

#include <amxmodx>
#include <sqlx>

/** Can we add kill to statrack */
stock bool:is_add_to_strack(id, csw) {
	new name[2][33];
	new wp_id = get_pdata_cbase(id, 373, 5);

	if ( !csw ||
	 	!g_player_stats[id][WP_SKIN][csw] ||
		!get_player_strack(id, csw)) {
			return false;
	}

	get_user_name(id, name[0], 32);
	get_weapon_owner( wp_id, name[1], 32);

	if (!equali(name[0], name[1]) && strlen(name[1]) >= 2) {
		return false;
	}

	return true
}

stock add_player_strack(id, weapon_id) {
	g_player_strack[id][weapon_id][WD_SKIN_TRACK] ++;
}

stock get_player_strack(id, weapon_id) {
	return g_player_strack[id][weapon_id][WD_SKIN_TRACK]
}
 
stock make_skin_options( skin_id, StatTrack = 0, OptionA = 0, OptionB = 0, output[], len) {
	return formatex(output, len, "%d_%d_%d_%d", skin_id, StatTrack, OptionA, OptionB)
}
 
stock oper_make_item( item[], len, item_array[ItemOptions]) {
	return make_skin_options( item_array[IO_ITEM_ID], item_array[IO_ITEM_STATTRACK], item_array[IO_ITEM_OPTIONA], item_array[IO_ITEM_OPTIONB], item, len)
}
stock print_oper_item(id, item_array[ItemOptions]) {
	console_print(id, "---------------")
	console_print(id, "ID: %d", item_array[IO_ITEM_ID])
	console_print(id, "STrack: %d", item_array[IO_ITEM_STATTRACK])
	console_print(id, "OptionA: %d", item_array[IO_ITEM_OPTIONA])
	console_print(id, "OptionB: %d", item_array[IO_ITEM_OPTIONB])
	console_print(id, "---------------")
}
stock oper_parse_item( item[], item_array[ItemOptions]) {
	return parse_skin_options(item, item_array[IO_ITEM_ID], item_array[IO_ITEM_STATTRACK], item_array[IO_ITEM_OPTIONA], item_array[IO_ITEM_OPTIONB]);
}

stock parse_skin_options( sSkin[], &skin_id, &statTrack, &optionA, &optionB ) {

	new copy_item[ITEM_MAX_SIZE];
	copy(copy_item, charsmax(copy_item), sSkin)

	skin_id = 0;
	statTrack = 0;
	optionA = 0;
	optionB = 0;

	if ( contain(copy_item, "_") == -1 ) {
		skin_id = str_to_num(copy_item)
		return skin_id
	}

	replace_all(copy_item, charsmax(copy_item), "_", " ")

	static s_skin[3];
	static s_track[5];
	static s_optionA[4];
	static s_optionB[4];
	parse(copy_item, s_skin, 2, s_track, 4, s_optionA, 3, s_optionB, 3)

	skin_id 	= str_to_num(s_skin)
	statTrack 	= str_to_num(s_track)
	optionA 	= str_to_num(s_optionA)
	optionB 	= str_to_num(s_optionB)

	return skin_id

}