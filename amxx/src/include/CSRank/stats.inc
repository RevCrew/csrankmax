#if defined _stats
	#endinput 
#endif

#define _stats

#include <amxmodx>
#include <amxmisc>

stock AddPlayerCase( id, amount) {
	g_player_stats[id][CASES]+=amount;
}

/*
stock AddPlayerWish(id, amount) {
	//g_player_stats[id][WISHES]+=amount;
} */

stock AddPlayerKey( id, amount) {
	g_player_stats[id][KEYS]+=amount;
}

stock AddPlayerExp( id, amount) {
	CoreExp_ExpUpdate(id, amount, true, false)
}

stock AddCoins(id, coins)
{
	g_player_stats[id][COINS] +=coins;
	//g_player_stats[id][TOTAL_COINS] +=coins;
}
stock RemoveCoins(id, coins)
{
	g_player_stats[id][COINS] -=coins;
}